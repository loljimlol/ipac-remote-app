package com.comfy.ipac;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {


    private static int state;
    private static Context ctx;
    private static final String PREFS_NAME = "IPACsets";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private String stream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, ContentFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.log_out) {
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("RadId", -1);
            editor.commit();
            finish();
        } else if (id == R.id.action_stop)
        {
            Object[] objs = new Object[]{6, null, null, null, null};
            new HttpGetAsync().execute(objs);
            Toast.makeText(this.getApplicationContext(), "Stopped stream", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.action_add)
        {
            switch (state)
            {
                case 1:
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

                    LinearLayout ll = new LinearLayout(this);
                    ll.setOrientation(LinearLayout.VERTICAL);

                    final EditText ed1 = new EditText(this);
                    ed1.setHint("Name of new stream");
                    final EditText ed2 = new EditText(this);
                    ed2.setHint("URL of new stream");
                    ll.addView(ed1);
                    ll.addView(ed2);
                    builder.setView(ll);

                    builder.setTitle("Add a stream");
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                            int ids = settings.getInt("RadId", -1);
                            Object[] objs = new Object[]{4, null, null, null, new Object[]{ed1.getText().toString(), ed2.getText().toString(), ids}};
                            new HttpGetAsync().execute(objs);
                            dialog.dismiss();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, ContentFragment.newInstance(state))
                                    .commit();
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

                    builder.show();
                    break;
                case 2:
                    builder = new AlertDialog.Builder(ctx);

                    ll = new LinearLayout(this);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    final EditText ed3 = new EditText(this);
                    ed3.setHint("Description of new alarm");
                    ll.addView(ed3);
                    final TextView tv = new TextView(this);
                    tv.setText("Time: 00:00");
                    final int[] time = new int[2];
                    tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TimePickerDialog diag = new TimePickerDialog(ctx, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int i, int i2) {
                                    time[0] = i;
                                    time[1] = i2;
                                    tv.setText("Time: " + ((i>9)?"":"0") + i + ":" + ((i2>9)?"":"0") + i2);
                                }
                            }, 0, 0, true);
                            diag.show();
                        }
                    });
                    ll.addView(tv);
                    final TextView tv2 = new TextView(this);
                    tv2.setText("Selected stream: none");
                    stream = "";
                    tv2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                            ArrayAdapter<String> ad =  new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, ContentFragment.items);
                            builder.setSingleChoiceItems(ad, 0, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    tv2.setText("Selected stream: " + ContentFragment.items.get(i)); //lol wtf am I doing
                                    stream = ContentFragment.items.get(i);
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.create();
                            builder.show();
                        }
                    });
                    ll.addView(tv2);
                    builder.setView(ll);

                    builder.setTitle("Add an alarm");
                    ctx = this;
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                            int ids = settings.getInt("RadId", -1);
                            if (stream!="") {
                                Object[] objs = new Object[]{5, null, null, null, new Object[]{ed3.getText().toString(), time, stream, ids}};
                                new HttpGetAsync().execute(objs);
                                dialog.dismiss();
                                FragmentManager fragmentManager = getFragmentManager();
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, ContentFragment.newInstance(state))
                                        .commit();
                            } else {

                            }
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

                    builder.show();
                    break;
            }


        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ContentFragment extends Fragment implements AdapterView.OnItemClickListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        public static ArrayList<String> items;


        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ContentFragment newInstance(int sectionNumber) {
            ContentFragment fragment = new ContentFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public ContentFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ctx = getActivity();
            //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            ListView lv = (ListView) rootView.findViewById(R.id.itemList);
            Object[] objs;
            state = getArguments().getInt(ARG_SECTION_NUMBER);
            if (state == 1) {
                items = new ArrayList<String>();
                objs = new Object[]{getArguments().getInt(ARG_SECTION_NUMBER), items, lv, this.getActivity()};
            }
            else
            {
                ArrayList items = new ArrayList<String>();
                objs = new Object[]{getArguments().getInt(ARG_SECTION_NUMBER), items, lv, this.getActivity()};
            }
            lv.setOnItemClickListener(this);
            new HttpGetAsync().execute(objs);


            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
            if (state == 1) {
                final Context ctx = this.getActivity();
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle("Confirm");
                builder.setMessage("Are you sure you want to play \"" + items.get(i) + "\"");
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Object[] objs = new Object[]{3, items, null, ctx, items.get(i)};
                        new HttpGetAsync().execute(objs);
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            }
        }
    }

}

class HttpGetAsync extends AsyncTask<Object, Void, String> {
    ArrayList<String> t;
    int type;
    String result = "fail";
    ListView lv;
    Context cxt;
    Object input;

    @Override
    protected String doInBackground(Object... params) {
        // TODO Auto-generated method stub
        this.t = (ArrayList<String>) params[1];
        this.type = (Integer) params[0];
        this.lv = (ListView) params[2];
        this.cxt = (Context) params[3];
        if (params.length > 4)
            input = params[4];
        Log.d("stuff", "integer is " + ((Integer) params[0]));
        return apiGET((Integer) params[0]);
    }

    final String apiGET(int type)
    {
        String query = "";
        switch (type)
        {
            case 1:
                query = "?q=stream";
                break;
            case 2:
                query = "?q=alarm";
                break;
            case 3:
                query = "?q=genconfig&name=" + URLEncoder.encode((String)input).replaceAll("\\+","%20");
                break;
            case 4:
                query = "?q=addstream&name=" + URLEncoder.encode((String)((Object[])input)[0]) + "&url=" + URLEncoder.encode((String)((Object[])input)[1]) + "&id=" + (Integer)((Object[])input)[2];
                break;
            case 5:
                query = "?q=addalarm&desc=" + URLEncoder.encode((String)((Object[])input)[0]) + "&stream=" + URLEncoder.encode((String)((Object[])input)[2]) + "&id=" + (Integer)((Object[])input)[3] + "&time=" + ((((int[])((Object[])input)[1])[0]>9)?"":"0") + ((int[])((Object[])input)[1])[0] + ":" + ((((int[])((Object[])input)[1])[1]>9)?"":"0") + ((int[])((Object[])input)[1])[1];
                break;
            case 6:
                query = "?q=genconfigstop";
                break;
        }
        String url = "http://vps.reupload.nl/cgi-bin/api.php" + query;
        BufferedReader inStream = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpRequest = new HttpGet(url);
            HttpResponse response = httpClient.execute(httpRequest);
            inStream = new BufferedReader(
                    new InputStreamReader(
                            response.getEntity().getContent()));

            StringBuffer buffer = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = inStream.readLine()) != null) {
                buffer.append(line + NL);
            }
            inStream.close();

            result = buffer.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    protected void onPostExecute(String page)
    {
        String key = "";
        switch (this.type){
            case 1:
                key = "name";
                break;
            case 2:
                key = "desc";
                break;
            default:
                return;
        }
        JSONArray jArray = null;
        try {
            jArray = new JSONArray(page);
            int len = jArray.length();
            for(int i = 0; i < len; i++) {
                JSONObject item = jArray.getJSONObject(i);
                t.add(item.getString(key));
            }
        } catch (JSONException e) {
            t.add("An internal error occurred");
            //e.printStackTrace();
        }
        lv.setAdapter(new ArrayAdapter<String>(cxt, android.R.layout.simple_list_item_1, t));


    }
}